#include <SoftwareSerial.h>

SoftwareSerial mySerial(1, 2); // RX, TX

const int Trigger = 3;   //Pin digital 2 para el Trigger del sensor
const int Echo = 4;   //Pin digital 3 para el Echo del sensor

void setup() {
  mySerial.begin(9600);//iniciailzamos la comunicación
  pinMode(Trigger, OUTPUT); //pin como salida
  pinMode(Echo, INPUT);  //pin como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void loop()
{

  long t; //timepo que demora en llegar el eco
  long d; //distancia en centimetros

  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          //Enviamos un pulso de 10us
  digitalWrite(Trigger, LOW);
  
  t = pulseIn(Echo, HIGH); //obtenemos el ancho del pulso
  d = t/59;             //escalamos el tiempo a una distancia en cm
  
  mySerial.print("Distancia: ");
  mySerial.print(d);      //Enviamos serialmente el valor de la distancia
  mySerial.print("cm");
  mySerial.println();
  // delay(300);          //Hacemos una pausa de 100ms
}
